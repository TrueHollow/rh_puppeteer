const logger = require('../logger')('services/UrlHandler.js');
const { GOTO_OPTIONS } = require('./ParserCommon');

class UrlHandler {
  /**
   * Constructor
   * @param {Browser} browser instance
   */
  constructor(browser) {
    this.browser = browser;
  }

  async run(url) {
    let page;
    let err;
    let type;
    let result;
    logger.info('Run invoked');
    try {
      page = await this.browser.getNewPage();
      // await page.setRequestInterception(true);
      // page.on('request', request => {
      //   request.continue();
      // });
      const response = await page.goto(url, GOTO_OPTIONS);
      const status = response.status();
      if (status === 200) {
        const headers = response.headers();
        const contentType = headers['content-type'];
        switch (contentType) {
          case 'text/html':
            result = await page.content();
            type = 'html';
            break;
          case 'application/pdf':
            // handled by custom code
            break;
          case 'text/xml':
          case 'application/xml':
            result = await page.evaluate(() => {
              const el = document.firstChild;
              if (!el) {
                console.error('First child element is not found!');
                return '';
              }
              return el.textContent.trim();
            });
            type = 'txt';
            break;
          default:
        }
      } else {
        throw new Error(`Http code: ${status}, url: ${url}`);
      }
    } catch (e) {
      if (e.message && e.message.indexOf('net::ERR_ABORTED') !== -1) {
        logger.debug(`Ignoring custom case (pdf): ${e.message}`);
        type = 'pdf';
      } else {
        logger.error(e);
        err = e;
      }
    } finally {
      if (page) {
        await page.close();
      }
    }
    if (err) {
      throw err;
    }
    return { result, type };
  }
}

module.exports = UrlHandler;
