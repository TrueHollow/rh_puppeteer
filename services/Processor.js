const config = require('../config');
const Browser = require('./Browser');
const IO = require('./IO');
const UrlHandler = require('./UrlHandler');

class Processor {
  constructor() {
    this.browser = new Browser(config.chrome);
    this.handler = null;
  }

  async init() {
    await Promise.all([IO.Init(), this.browser.init()]);
    this.handler = new UrlHandler(this.browser);
  }

  async run(url) {
    const result = this.handler.run(url);
    await IO.WaitDownloadsFinish();
    await IO.ProcessPdfFiles();
    return result;
  }

  async finish() {
    await this.browser.finish();
  }
}

module.exports = Processor;
