const logger = require('../logger')('service/ParserCommon.js');

function getRandomInt(min, max) {
  const minimum = Math.ceil(min);
  const maximum = Math.floor(max);
  return Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
}

const TYPE_MINIMUM_DELAY = 30;
const TYPE_MAXIMUM_DELAY = 50;
const MAX_ATTEMP_COUNT = 3;
const CSS_SELECTOR_TIMEOUT = 6000;

const delay = async timeOut => {
  logger.debug('Delay: (%d)', timeOut);
  return new Promise(resolve => {
    setTimeout(resolve, timeOut);
  });
};

const getLongDelay = ({ minDelay, maxDelay }) => {
  return getRandomInt(minDelay, maxDelay);
};

const createInputDelay = () => {
  return {
    delay: getRandomInt(TYPE_MINIMUM_DELAY, TYPE_MAXIMUM_DELAY),
  };
};

const GOTO_OPTIONS = { waitUntil: 'networkidle0' };

module.exports = {
  TYPE_MINIMUM_DELAY,
  TYPE_MAXIMUM_DELAY,
  MAX_ATTEMP_COUNT,
  createInputDelay,
  delay,
  GOTO_OPTIONS,
  getLongDelay,
  CSS_SELECTOR_TIMEOUT,
};
