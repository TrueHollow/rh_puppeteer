const puppeteer = require('puppeteer-extra');
const pluginStealth = require('puppeteer-extra-plugin-stealth');

const IO = require('./IO');

puppeteer.use(pluginStealth());

puppeteer.use(
  require('puppeteer-extra-plugin-user-preferences')({
    userPrefs: {
      download: {
        prompt_for_download: false,
        open_pdf_in_system_reader: false,
        default_directory: IO.OutputDirectory(),
      },
      plugins: {
        always_open_pdf_externally: true, // this should do the trick
      },
    },
  })
);

const logger = require('../logger')('service/Browser.js');

class Browser {
  constructor(config) {
    this.config = config;
    this.browser = null;
  }

  async init() {
    if (this.browser == null) {
      logger.info('Initialize browser instance.');
      this.browser = await puppeteer.launch(this.config);
    }
  }

  async getNewPage() {
    const page = await this.browser.newPage();
    page.on('console', msg =>
      logger.debug('PAGE LOG:', msg.type(), msg.text())
    );
    await page.setViewport({ width: 1920, height: 1040 });
    return page;
  }

  async finish() {
    if (this.browser) {
      logger.info('Closing browser.');
      await this.browser.close();
    }
  }
}

module.exports = Browser;
