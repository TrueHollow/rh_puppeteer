const fs = require('fs');
const path = require('path');
const pdf2html = require('pdf2html');
const logger = require('../logger')('services/IO.js');
const { delay } = require('./ParserCommon');

const OUTPUT_DIRECTORY = path.resolve(__dirname, '../output');

const createOutputDirectory = async () => {
  return new Promise((resolve, reject) => {
    fs.mkdir(`${OUTPUT_DIRECTORY}`, { recursive: true }, err => {
      if (err) {
        return reject(err);
      }
      logger.debug('Output directory was created.');
      return resolve();
    });
  });
};

const writeToOutput = async (fileName, data, type = 'html') => {
  const fullPath = path.resolve(OUTPUT_DIRECTORY, `${fileName}.${type}`);
  return new Promise((resolve, reject) => {
    fs.writeFile(fullPath, data, err => {
      if (err) {
        return reject(err);
      }
      return resolve();
    });
  });
};

const REG_UNFINISHED = /\.crdownload$/i;
const REG_PDF = /\.pdf$/i;

const getFilesInOutput = async () => {
  return new Promise((resolve, reject) => {
    fs.readdir(OUTPUT_DIRECTORY, (err, files) => {
      if (err) {
        return reject(err);
      }
      return resolve(files);
    });
  });
};

const convertPDFtoHTML = async filePath => {
  return new Promise((resolve, reject) => {
    pdf2html.html(filePath, (err, html) => {
      if (err) {
        return reject(err);
      }
      return resolve(html);
    });
  });
};

const readFile = async filePath => {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, { encoding: 'utf8' }, (err, data) => {
      if (err) {
        return reject(err);
      }
      return resolve(data);
    });
  });
};

const renameFile = async (fileName, newName) => {
  return new Promise((resolve, reject) => {
    const fromFile = path.join(OUTPUT_DIRECTORY, fileName);
    const toFile = path.join(OUTPUT_DIRECTORY, newName);
    fs.rename(fromFile, toFile, err => {
      if (err) {
        return reject(err);
      }
      return resolve(toFile);
    });
  });
};

const deleteFile = async filePath => {
  return new Promise((resolve, reject) => {
    fs.unlink(filePath, err => {
      if (err) {
        return reject(err);
      }
      return resolve();
    });
  });
};

class IO {
  static OutputDirectory() {
    return OUTPUT_DIRECTORY;
  }

  static async Init() {
    logger.info('Init output folder.');
    return createOutputDirectory();
  }

  static async Save(fileName, html, type) {
    logger.info('Save html output');
    await writeToOutput(fileName, html, type);
  }

  static async WaitDownloadsFinish() {
    let files = [];
    await delay(3000);
    do {
      // eslint-disable-next-line no-await-in-loop
      await delay(1000);
      // eslint-disable-next-line no-await-in-loop
      files = (await getFilesInOutput()).filter(f => REG_UNFINISHED.test(f));
    } while (files.length);
    logger.info('Downloads are finished.');
  }

  static async ProcessPdfFiles() {
    const pdfs = (await getFilesInOutput()).filter(f => REG_PDF.test(f));
    let count = 0;
    const promises = pdfs.map(async f => {
      count += 1;
      const fullPath = await renameFile(f, `${count}.pdf`); // to avoid issues related jar argument
      const html = await convertPDFtoHTML(fullPath);
      await writeToOutput(f, html, 'html');
      return deleteFile(fullPath);
    });
    await Promise.all(promises);
  }

  static async ReadInputFile() {
    const filePath = path.join(OUTPUT_DIRECTORY, 'input.txt');
    let result = [];
    try {
      const data = await readFile(filePath);
      result = data
        .split('\n')
        .map(s => s.trim())
        .filter(s => s.length > 0);
    } catch (e) {
      if (e.code === 'ENOENT') {
        logger.debug('Input file not found');
      } else {
        logger.error(e);
      }
    }
    return result;
  }
}

module.exports = IO;
