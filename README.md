# URL handler

## System requirements:

Node.js version 10 or higher. 
Java runtime environment (JRE) is required to run [this module (convert PDF to HTML).](https://github.com/shebinleo/pdf2html#readme)

## Prepare

`npm i --production`

## Run

`npm start`
