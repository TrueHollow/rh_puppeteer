const chai = require('chai');
const Processor = require('../services/Processor');

const { expect } = chai;

let processor;
describe('Test Accounts http methods', () => {
  before(async () => {
    processor = new Processor();
    await processor.init();
  });
  after(async () => {
    if (processor) {
      await processor.finish();
    }
  });
  it('test PDF', async () => {
    const { result, type } = await processor.run(
      'http://www.pdf995.com/samples/pdf.pdf'
    );
    expect(result).to.be.undefined;
    expect(type)
      .to.be.a('string')
      .and.to.equal('pdf');
  });
  it('test PDF (data.oregon.gov)', async () => {
    const { result, type } = await processor.run(
      'https://data.oregon.gov/api/views/hmp5-cmyh/rows.pdf?app_token=U29jcmF0YS0td2VraWNrYXNz0'
    );
    expect(result).to.be.undefined;
    expect(type)
      .to.be.a('string')
      .and.to.equal('pdf');
  });
  it('test XML', async () => {
    const { result, type } = await processor.run(
      'https://www.hl7.org/fhir/endpoint-example.xml'
    );
    expect(result)
      .to.be.a('string')
      .and.to.equal(
        'Health Intersections CarePlan Hub\n\t\t\tCarePlans can be uploaded to/from this loccation'
      );
    expect(type)
      .to.be.a('string')
      .and.to.equal('txt');
  });
  it('test PDF (with redirection)', async () => {
    const { result, type } = await processor.run(
      'https://agenda.okc.gov/sirepub/view.aspx?cabinet=published_meetings&fileid=4058303'
    );
    expect(result).to.be.undefined;
    expect(type)
      .to.be.a('string')
      .and.to.equal('pdf');
  });
  it('test 404 (should throw)', async () => {
    let err;
    try {
      await processor.run('https://nwmi.org/gala-2019');
    } catch (e) {
      err = e;
    }
    expect(err).to.be.not.undefined;
  });
});
