const IO = require('./services/IO');
const Processor = require('./services/Processor');
const logger = require('./logger')('index.js');

logger.info('Script is started');

let processor;
const main = async () => {
  processor = new Processor();
  logger.info(`CLASSPATH: '${process.env.CLASSPATH}'`);
  await processor.init();
  const urls = await IO.ReadInputFile();
  logger.info(`Proceed: ${urls.length} urls.`);
  // eslint-disable-next-line no-restricted-syntax
  for (const url of urls) {
    try {
      // eslint-disable-next-line no-await-in-loop
      const { result, type } = await processor.run(url);
      if (result) {
        // eslint-disable-next-line no-await-in-loop
        await IO.Save('output', result, type);
      }
    } catch (e) {
      logger.error(e);
    }
  }
};

main().then(async () => {
  logger.info('Script is finished');
  if (processor) {
    await processor.finish();
  }
});
